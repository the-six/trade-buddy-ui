import React from 'react';
import Cash from '../../components/UserAccount/UserAccount';
import SecuritiesSearchBar from '../../components/SecuritiesSearchBar/SecuritiesSearchBar';
import SecuritiesTable from '../../components/SecuritiesTable/SecuritiesTable';
import WatchList from '../../components/WatchList/WatchList';
import TradeBuddyLogo from '../../assets/images/trade-buddy.svg'
import { TransactionProvider } from '../../components/TransactionProvider/TransactionProvider'

import "./Overview.css";

const Overview = () => {
    return (
        <div className="overview-container">
            <TransactionProvider>
                <div className="col-1">
                    <Cash />
                    <img src={TradeBuddyLogo} alt="trade-buddy" className="trade-buddy-logo"/>
                </div>
                <div className="col-space" />
                {/* Graph + Transaction Column*/}
                <div className="col-2">
                    <SecuritiesSearchBar />
                    <SecuritiesTable />
                </div>
                <div className="col-space" />
                {/* Watchlist Column */}
                <div className="col-3">
                    <WatchList />
                </div>
            </TransactionProvider>
        </div>
    )
}

export default Overview;