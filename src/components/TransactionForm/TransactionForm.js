import React, { useState } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import './TransactionForm.css';

const serverUrl = "http://trade-buddy-backend-trade-buddy-backend.dev6.benivade.com/";

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%',
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));


const TransactionForm = ({ symbol, currentPrice }) => {
    const classes = useStyles();
    const [quantity, setQuantity] = useState(0.0);
    const [account, setAccount] = useState("");
    const [total, setTotal] = useState(0.0);

    function refreshTotal(entryPrice, quantity) {
        setTotal((entryPrice*quantity).toFixed(2));
    }

    const onSubmit = async () => {
        const url = serverUrl + "api/v1/transaction";
        const body = {
            "ticker": symbol,
            "entryPrice": currentPrice,
            "currentPrice": currentPrice,
            "quantity": quantity,
            "timestamp": Date.now()
        }

        try {
            const response = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            });

            const responseJson = await response.json();
        } catch (error) {
            console.log("Failed to submit transaction");
        }
    }

    return (
        <div>
            <form className={classes.form}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}> 
                        <TextField id="ticker" label="Ticker" variant="outlined"
                            value={symbol}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField id="account" label="Account" variant="outlined"
                            onInput={(e) => setAccount(e.target.value)}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}> 
                        <TextField id="entry-price" label="Entry Price" variant="outlined"
                            value={currentPrice}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}> 
                        <TextField id="quantity" label="Quantity" variant="outlined"
                            onInput={(e) => {
                                setQuantity(e.target.value);
                                refreshTotal(currentPrice, e.target.value);
                            }}
                        />
                    </Grid>
                </Grid>
                <div className="estimated-total-container">
                    <div className="estimated-total">$ {total}</div>
                </div>
                <div className="submit-button-container">
                    <Button className="submit-button" onClick={onSubmit}>
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default TransactionForm;