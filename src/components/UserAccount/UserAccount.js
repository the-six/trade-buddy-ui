import React from 'react';
import List from '@material-ui/core/List';
import BalanceAccount from '../Accounts/BalanceAccount.js';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from "@material-ui/icons/Refresh";

import AddCashHoldingsModal from '../AddCashHoldingsModal/AddCashHoldingsModal.js';
import { TransactionContext } from '../TransactionProvider/TransactionProvider'
import Grid from '@material-ui/core/Grid'


import './UserAccount.css';

function addAccount(id, financialService, currency, balance) {
    return { id, financialService, currency, balance };
}

const rows = [
    addAccount('4512352', 'RBC Investing', 'USD', 15000.00),
    addAccount('5432152', 'RBC Investing', 'CAD', 30000.00),
    addAccount('1813511', 'BOE', 'GBP', 17000.00)
];

const serverUrl = "http://trade-buddy-backend-trade-buddy-backend.dev6.benivade.com/";

const UserAccount = () => {
    const [accounts, setAccounts] = React.useState(rows);
    const [overallBalance, setOverallBalance] = React.useState(0.0);
    const { transactions } = React.useContext(TransactionContext);
    const [ totalTransactions, setTotalTransactions ] = React.useState(0.0)

    const onUpdateAccounts = async () => {
        const url = serverUrl + "api/v1/cashholding";

        try {
        const response = await fetch(url, {
            method: "GET",
            headers: {
            "Content-Type": "application/json"
            }
        });

        const accountsJson = await response.json();
        setAccounts(accountsJson);
        
        let balanceSum = 0.0;
        accountsJson.forEach((account) => {
            balanceSum += account.balance;
        });

        let transactionsTotal = 0.0;
        transactions.forEach((transaction) => {
            transactionsTotal += (transaction.quantity * transaction.entryPrice)
        })

        transactionsTotal = transactionsTotal.toFixed(2)

        setTotalTransactions(transactionsTotal)

        console.log(transactionsTotal)

        setOverallBalance((balanceSum - transactionsTotal).toFixed(2));

        } catch (error) {
        console.log("Failed to query accounts");
        }
    }
    
    const renderBalanceAccountCard = accounts => (
        <BalanceAccount {...accounts}
            key={accounts.id}
            />
    );

    return (
        <div className="root">
            <List>
                <div className="title-container">
                    <div className="title">
                        Cash
                    </div>
                    <div className="user-button-container">
                        <IconButton onClick={onUpdateAccounts}>
                            <RefreshIcon />
                        </IconButton>
                        <AddCashHoldingsModal />
                    </div>
                </div>
                {accounts.map(renderBalanceAccountCard)}
                <div className="account-card">
                    <Grid container spacing={1} justify="center">
                        <Grid item xs={6} direction="column">
                            <div className="financial-service">
                                Transactions Total
                            </div>
                        </Grid>
                        <Grid item xs={6} direction="column">
                            <div className="account-balance">
                                ${totalTransactions}
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </List>
            
            
            <div className="cash-total">Your Current Balance: ${overallBalance} USD</div>
        </div>
    )
}
export default UserAccount;