import React, { useState } from 'react';

import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';

import RefreshIcon from '@material-ui/icons/Refresh';

import './WatchList.css';
import WatchListItem from '../WatchListItem/WatchListItem';

function addItem(id, ticker, name, exchange, amount) {
    return { id, ticker, name, exchange, amount };
}

const watchlist = [
    addItem('123098754', 'AMZN', 'Amazon', 'NASDAQ', 2000.00),
    addItem('120893475', 'GOOGL', 'Alphabet Inc.', 'NASDAQ', 2000.00),
    addItem('kaojsdhfsa','AC', 'Air Canada', 'TSE', 20.00)
];

const WatchList = () => {
    const [items, setItems] = useState(watchlist);

    function refreshWatchList() {

    }

    const renderWatchListItem = item => (
        <WatchListItem {...item}
            key={item.id}
            />
    );

    return (
        <div className="watchlist-container">
            <div className="title-container">
                <div className="watchlist-title">
                    Watch List
                </div>
                <IconButton onClick={refreshWatchList}>
                    <RefreshIcon />
                </IconButton> 
            </div>
            <List component="nav" aria-label="main mailbox folders">
                {items.map(renderWatchListItem)}
            </List>
        </div>
    );
}

export default WatchList;