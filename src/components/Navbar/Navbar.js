import React from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import TradeBuddyImg from '../../assets/images/trade-buddy-head.png';
import './Navbar.css';

const NavBar = () => {
    return (
        <div className="navbar-container">
            <div className="navbar-home">
                <div className="navbar-title">
                    <img className="navbar-img" src={TradeBuddyImg} alt="logo" />
                    tradeBuddy
                </div>
            </div>
            <div className="navbar-spacer" />
            <div className="navbar-group">
                <a className="navbar-item" href="/"> Overview </a>
                <a className="navbar-item" href="/"> Cash </a>
                <a className="navbar-item" href="/"> Portfolio </a>
                <a className="navbar-item" href="/"> Watchlist </a>
            </div>
            <div className="navbar-spacer" />
            <div className="navbar-account-group">
                <div> <AccountCircleIcon/> John Smith </div>
            </div>
        </div>
    )
}

export default NavBar;