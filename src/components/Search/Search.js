import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import Modal from '@material-ui/core/Modal';
import { Button } from '@material-ui/core';

import './SecuritiesSearchBar.css';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  input: {
    color: 'white'
  }
});

function createData(symbol, currentPrice) {
  return { symbol, currentPrice };
}

const Search = () => {
  const classes = useStyles();
  const [prices, setPrices] = React.useState([]);
  const [search, setSearch] = React.useState('');
  const [open, setOpen] = React.useState(false);

  // eslint-disable-next-line
  const onSearch = async (symbol) => {

    var options = {
      method: 'GET',
      url: 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes',
      params: {region: 'US', symbols: symbol},
      headers: {
        'x-rapidapi-key': '4a7cace49amsh213ffe783076209p1d2a1cjsne58317c9f247',
        'x-rapidapi-host': 'apidojo-yahoo-finance-v1.p.rapidapi.com'
      }
    };
    
    axios.request(options).then(function (response) {
      console.log(response.data);
      
      const stocks = response.data.quoteResponse.result.map((val) => {
        return createData(val.symbol, val.ask)
      });

      setPrices(stocks)
    }).catch(function (error) {
      console.error(error);
    });

    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const onBuyButtonPress = (symbol, price) => {
    console.log(symbol, price)
  }

  return (
    <div>
      <Input
        id="search"
        placeholder="Search"
        className={classes.input}
        value={search}
        onChange={(e) => {
          setSearch(e.target.value)
        }}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            onSearch(search)
          }
        }}
      />
      <Modal
        open={open}
        onClose={onClose}
      >
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Symbol</TableCell>
                <TableCell align="left">Price ($)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {prices.map((security) => (
                <TableRow key={security.symbol}>
                  <TableCell align="left">
                    {security.symbol}
                  </TableCell>
                  <TableCell align="left">{security.currentPrice}</TableCell>
                  <TableCell align="right"><Button onClick={() => {onBuyButtonPress(security.symbol, security.currentPrice)}}>Buy</Button></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Modal>
    </div>
  )
};

export default Search;
