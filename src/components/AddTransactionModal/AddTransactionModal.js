import React, { useState } from 'react'
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

import TransactionForm from '../TransactionForm/TransactionForm';

import './AddTransactionModal.css'

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

const AddTransactionModal = ({ open, onClose, security }) => {

    return (
        <div>
            <Modal
                isOpen={open}
                onRequestClose={onClose}
                style={customStyles}
            >
                <div className="title-container">
                    <h3 className="securities-title">Add New Transaction</h3>
                    <div className="securities-title-buttons">
                        <IconButton onClick={onClose}>
                            <CloseIcon />
                        </IconButton>
                    </div>
                </div>
                {security ? <TransactionForm symbol={security.symbol} currentPrice={security.currentPrice} /> : ''}
            </Modal>
        </div>
    )
};

export default AddTransactionModal;