import React from 'react';
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography';

import NumberFormat from 'react-number-format';

import './BalanceAccount.css';

const BalanceAccount = (accounts) => {
    return (
        <div className="account-card">
            <Grid container spacing={1} justify="center">
                <Grid item xs={6} direction="column">
                    <div className="financial-service">
                        {accounts.financialService}
                    </div>
                    <Typography className="pos" color="textSecondary">
                        {accounts.currency}
                    </Typography>
                </Grid>
                <Grid item xs={6} direction="column">
                    <div className="account-balance">
                        <NumberFormat value={accounts.balance} displayType={'text'} thousandSeparator={true} prefix={'$'} />
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}
export default BalanceAccount;