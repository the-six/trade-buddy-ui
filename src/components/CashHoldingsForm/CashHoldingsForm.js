import React, { useState } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import "./CashHoldingsForm.css";

const serverUrl = "http://trade-buddy-backend-trade-buddy-backend.dev6.benivade.com/";

const useStyles = makeStyles((theme) => ({
    form: {
      width: '100%',
      marginTop: theme.spacing(3),
    },
  }));

const CashHoldingsForm = () => {
    const classes = useStyles();

    const [financialService, setFinancialService] = useState("");
    const [balance, setBalance] = useState(0.0);
    const [currency, setCurrency] = useState("");

    const onSubmit = async () => {
        console.log(financialService);
        console.log(balance);
        console.log(currency);
        const url = serverUrl + "api/v1/cashholding";
        const body = {
            "financialService": financialService,
            "balance": balance,
            "currentPrice": balance,
            "currency": currency,
            "timestamp": Date.now()
        }

        try {
            const response = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body)
            });

            const responseJson = await response.json();

            setFinancialService("");
            setBalance(0.0);
            setCurrency("");
        } catch (error) {
            console.log("Failed to submit cash account");
        }
    }

    return (
        <div className="cash-account-container">
            <form className={classes.form}>
                <Grid container spacing={2}>
                    <Grid item xs={12}> 
                        <TextField id="financialService" label="Financial Service" variant="outlined"
                            onInput={(e) => setFinancialService(e.target.value)}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}> 
                        <TextField id="balance" label="Balance" variant="outlined" fluid fullWidth
                            onInput={(e) => setBalance(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}> 
                    <TextField id="currency" label="Currency" variant="outlined"
                        onInput={(e) => setCurrency(e.target.value)}
                    />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12} >
                        <Button className="submit-button" variant="contained" color="primary" onClick={onSubmit}>
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </div>
    )
}

export default CashHoldingsForm;