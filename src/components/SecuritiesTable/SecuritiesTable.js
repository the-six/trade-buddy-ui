import React, { useEffect } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import RefreshIcon from '@material-ui/icons/Refresh';
import axios from 'axios';
import { TransactionContext } from '../TransactionProvider/TransactionProvider'

import "./SecuritiesTable.css";

function createData(id, stockTicker, entryPrice, quantity) {
  return { id, stockTicker, entryPrice, quantity };
}

const rows = [
  createData('1092719', 'AMZN', 3648.04, 3.52),
  createData('1129873', 'GOOGL', 2622.16, 1.20),
];

const serverUrl = "http://trade-buddy-backend-trade-buddy-backend.dev6.benivade.com/";

const SecuritiesTable = () => {
  const [prices, setPrices] = React.useState({});
  const { transactions, setTransactions } = React.useContext(TransactionContext);
  console.log(transactions)

  // eslint-disable-next-line
  const onUpdateSecurities = async () => {
    const url = serverUrl + "api/v1/transaction";

    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      });

      const securitiesJson = await response.json();
      setTransactions(securitiesJson);
      console.log(securitiesJson)
      const temp = securitiesJson.map((val) => {
        return val.ticker;
      })


      const symbols = [...new Set(temp)]

      var options = {
        method: 'GET',
        url: 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes',
        params: {region: 'US', symbols: symbols.toString()},
        headers: {
          'x-rapidapi-key': '4a7cace49amsh213ffe783076209p1d2a1cjsne58317c9f247',
          'x-rapidapi-host': 'apidojo-yahoo-finance-v1.p.rapidapi.com'
        }
      };
      
      axios.request(options).then(function (response) {
        
        let stockPrices = {}
        response.data.quoteResponse.result.map((val) => {
          stockPrices[val.symbol] = val.ask;
        });
        setPrices(stockPrices);
        console.log(response)
      }).catch(function (error) {
        console.error(error);
      });

    } catch (error) {
      console.log("Failed to query securities");
    }
  }

  console.log(prices)

  const getChange = (security) => {
    const currentPrice = (security.ticker in prices ? prices[security.ticker] : 0) * security.quantity
    const entryPrice = security.entryPrice * security.quantity

    const change = entryPrice <= 0 ? 0.00 : (((currentPrice - entryPrice) / entryPrice) * 100).toFixed(2)

    return change <= 0 ? (<TableCell align="right" style={{ color: "red" }}>{change}%</TableCell>) : (<TableCell align="right" style={{ color: "green" }}>{change}%</TableCell>)
  }

  return (
    <div className="securities-container">
      <div className="securities-title-container">
        <div className="securities-title">
          Transactions
        </div>
        <div className="securities-title-buttons">
          <IconButton onClick={onUpdateSecurities} >
            <RefreshIcon />
          </IconButton>
        </div>
      </div>
      <TableContainer component={Paper}>
        <Table className="securities-table" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>StockTicker</TableCell>
              <TableCell align="right">Entry ($)</TableCell>
              <TableCell align="right">Current ($)</TableCell>
              <TableCell align="right">Quantity</TableCell>
              <TableCell align="right">Change</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {transactions.map((security) => (
              <TableRow key={security.id}>
                <TableCell component="th" scope="row">
                  {security.ticker}
                </TableCell>
                <TableCell align="right">{security.entryPrice}</TableCell>
                <TableCell align="right">{security.ticker in prices ? prices[security.ticker] : 0}</TableCell>
                <TableCell align="right">{security.quantity}</TableCell>
                {getChange(security)}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
};

export default SecuritiesTable;