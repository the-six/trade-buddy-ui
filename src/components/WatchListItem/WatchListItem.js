import React from 'react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import NumberFormat from 'react-number-format';

import './WatchListItem.css';

const WatchListItem = (items) => {
    return (
        <div className="item-container">
            <Grid container spacing={1} justify="center">
                <Grid item xs={6} direction="column">
                    <div className="ticker">
                        {items.ticker}
                    </div>
                    <Typography className="pos" color="textSecondary">
                        {items.name}
                    </Typography>
                </Grid>
                <Grid item xs={6} direction="column">
                    <div className="account-balance">
                        <NumberFormat value={items.amount} displayType={'text'} thousandSeparator={true} prefix={'$'} />
                    </div>
                    <div className="exchange">
                        {items.exchange}
                    </div>
                </Grid>
            </Grid>
        </div>
    )
};

export default WatchListItem;