import React, { useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

import Modal from 'react-modal';

import CashHoldingsForm from '../CashHoldingsForm/CashHoldingsForm';

import './AddCashHoldingsModal.css';

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

const AddCashHoldingsModal = () => {
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <div>
            <IconButton onClick={openModal}>
                <AddIcon />
            </IconButton>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
            >
                <div className="modal-title-container">
                    <div className="modal-title"> Add New Cash Account </div>
                    <IconButton className="modal-close" onClick={closeModal}>
                        <CloseIcon />
                    </IconButton>
                </div>
                <CashHoldingsForm />
            </Modal>
        </div>
    );
}

export default AddCashHoldingsModal;