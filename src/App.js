import NavBar from './components/Navbar/Navbar';
import Overview from './pages/Overview/Overview';

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <NavBar />
        <Overview />
      </header>
    </div>
  );
}

export default App;
